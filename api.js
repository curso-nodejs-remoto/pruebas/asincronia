function getLastId(cb) {
  // Devuelve la última id de usuario introducida en la BD
  setTimeout(() => {
    // Ha ido todo bien
    const id = 20;
    cb(null, id);
    // Hay un error
    // const error = new Error("No se ha podido obtener la última id");
    // cb(error);
  }, 2000);
}

function getUsuario(id, cb) {
  // Devuelve un usuario a partir de su id
  setTimeout(() => {
    // Ha ido todo bien
    const usuario = {
      id: 20,
      nombre: "Juan",
      edad: 36,
      dni: "2877477234T"
    };
    cb(null, usuario);
    // Hay un error
    /*     const error = new Error(`No existe un usuario con la id ${id}`);
    cb(error); */
  }, 2500);
}

function getImportesMultas(dni, cb) {
  // Devuelve un array con los importes de las multas asociadas a un DNI
  setTimeout(() => {
    const importes = [100, 40, 20, 1500];
    cb(null, importes);
  }, 2000);
}

module.exports = {
  getLastId,
  getUsuario,
  getImportesMultas
};
