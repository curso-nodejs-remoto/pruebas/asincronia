const chalk = require("chalk");
const { hazCosas } = require("./utils");
const {
  getLastId, getUsuario, getImportesMultas, getPronostico
} = require("./api-promesas");

console.log("Mi aplicación empieza a funcionar");

async function cargaImportesMultasUsuario() {
  try {
    const ultimaId = await getLastId();
    console.log(`Se ha recibido la id ${ultimaId}`);
    console.log("Vamos a pedir el usuario con id", ultimaId);
    const usuario = await getUsuario(ultimaId).catch(err => {
      console.log(chalk.red.bold("No hemos obtenido el usuario"));
      throw err;
    });
    console.log("Se ha recibido el usuario", usuario);
    console.log("Vamos a pedir los importes de las multas del DNI", usuario.dni);
    const importes = await getImportesMultas(usuario.dni);
    console.log("Se han recibido los importes", importes);
  } catch (err) {
    console.log(chalk.red.bold("ERROR: ", err.message));
  }
}
cargaImportesMultasUsuario();
