const chalk = require("chalk");
const { getLastId, getUsuario, getImportesMultas } = require("./api");

// Obtenemos la última id de usuario de la BD
console.log("Pidiendo la última id a la BD");
getLastId((err, ultimaId) => {
  if (err) {
    console.log(chalk.red.bold("ERROR: ", err.message));
    return;
  }
  console.log(`Se ha recibido la id ${ultimaId}`);
  console.log(`Pidiendo a la BD el usuario con id ${ultimaId}`);
  getUsuario(ultimaId, (err, usuario) => {
    if (err) {
      console.log(chalk.red.bold("ERROR: ", err.message));
      return;
    }
    console.log("Se ha recibido el usuario", usuario);
    getImportesMultas(usuario.dni, (err, importes) => {
      if (err) {
        console.log(chalk.red.bold("ERROR: ", err.message));
        return;
      }
      console.log("Se han recibido los importes", importes);
    });
  });
});
