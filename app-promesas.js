const chalk = require("chalk");
const {
  getLastId, getUsuario, getImportesMultas, getPronostico
} = require("./api-promesas");

// Obtenemos la última id de usuario de la BD
console.log("Pidiendo la última id a la BD");
getLastId()
  .then(ultimaId => {
    console.log(`Se ha recibido la id ${ultimaId}`);
    console.log("Vamos a pedir el usuario con id", ultimaId);
    return getUsuario(ultimaId);
  })
  .catch(err => {
    console.log(chalk.red.bold("Ha fallado la obtención de la id"));
    throw err;
  })
  .then(usuario => {
    console.log("Se ha recibido el usuario", usuario);
    console.log("Vamos a pedir los importes de las multas del DNI", usuario.dni);
    return getImportesMultas(usuario.dni);
  })
  .then(importes => {
    console.log("Se han recibido los importes", importes);
  })
  .catch(err => {
    console.log(chalk.red.bold("ERROR: ", err.message));
  });

const pronostico = getPronostico();

/* const prom1 = getLastId();
const prom2 = getPronostico();

Promise.all([getLastId(), getPronostico()]).then(resultados => {
  const ultimaId = resultados[0];
  const pronostico = resultados[1];
  console.log(`La última id es ${ultimaId} y el pronóstico para mañana es ${pronostico}`);
}).catch(err => {
  console.log(chalk.red.bold("ERROR: ", err.message));
}); */
