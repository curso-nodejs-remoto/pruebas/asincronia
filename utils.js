function getPronostico(hora, cb) {
  setTimeout(() => {
    cb("soleado");
  }, 2500);
}

function hazCosas() {
  const mensajes = ["Sigo trabajando", "Sigo haciendo mis cosas", "Por aquí todavía haciendo cosas", "Sigo trabajando", "Sigo haciendo mis cosas", "Por aquí todavía haciendo cosas", "Sigo trabajando", "Sigo haciendo mis cosas", "Por aquí todavía haciendo cosas"];
  setInterval(() => {
    console.log(mensajes[Math.floor(Math.random() * 10)]);
  }, 1000);
}

const x = 10;
const y = 20;

const objeto = {
  x,
  y
};

module.exports = {
  getPronostico,
  hazCosas
};
