function getLastId() {
  // Devuelve la última id de usuario introducida en la BD
  console.log("Pidiendo última id");
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // Ha ido todo bien
      const id = 20;
      console.log("Obtenida última id");
      resolve(id);
      // Hay un error
      /* const error = new Error("No se ha podido obtener la última id");
      reject(error); */
    }, 3500);
  });
}

function getUsuario(id) {
  return new Promise((resolve, reject) => {
    // Devuelve un usuario a partir de su id
    setTimeout(() => {
      // Ha ido todo bien
      /* const usuario = {
        id: 20,
        nombre: "Juan",
        edad: 36,
        dni: "2877477234T"
      };
      resolve(usuario); */
      // Hay un error
      const error = new Error(`No existe un usuario con la id ${id}`);
      reject(error);
    }, 2500);
  });
}

function getImportesMultas(dni) {
  return new Promise((resolve, reject) => {
    // Devuelve un array con los importes de las multas asociadas a un DNI
    setTimeout(() => {
      const importes = [100, 40, 20, 1500];
      resolve(importes);
    }, 2000);
  });
}

function getPronostico() {
  console.log("Pidiendo pronóstico");
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // Todo ha ido bien
      console.log("Pronóstico recibido");
      resolve("soleado");
      // Ha habido un error
      reject(new Error("No se ha podido obtener el pronóstico"));
    }, 2000);
  });
}

module.exports = {
  getLastId,
  getUsuario,
  getImportesMultas,
  getPronostico
};
